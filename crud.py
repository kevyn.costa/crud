import psycopg2
from tkinter import messagebox, simpledialog 
import tkinter as tk

#CRUD POSTGRES
#Autor: Kevyn
options_menu=['-----Inserir','-----Atualizar','-----Deletar','-----Visualizar']

def insert():
    conexion=psycopg2.connect(user='postgres', password='password', host='127.0.0.1', port='5432', database='userdb')
    cursor=conexion.cursor()
    
    sql='INSERT INTO pessoas (nome, idade) VALUES(%s,%s)'

    ROOT = tk.Tk()

    ROOT.withdraw()
   
# the input dialog
    nome = simpledialog.askstring(title="Cadastrar dados",prompt="Digite o nome: ")
    idade=simpledialog.askinteger(title="Cadastrar dados",prompt="Digite a idade ")
    if nome and idade != "":
            dados=(nome,idade)
            cursor.execute(sql,dados)
            conexion.commit()
    #registro=cursor.rowcount
            messagebox.showinfo('Situção do Cadastro','Cadastro Concluído')
            cursor.close()
            conexion.close()
    else:
        messagebox.showerror('Situção do Cadastro','Cadastro não Concluído')



def update():
    conexion=psycopg2.connect(user='postgres', password='password', host='127.0.0.1', port='5432', database='db')

    cursor=conexion.cursor()
    id = simpledialog.askinteger(title="Cadastrar dados",prompt="Digite id ")
    sql='UPDATE pessoas SET nome=%s,idade=%s WHERE id=%s'
    nome=simpledialog.askstring(title="Cadastrar dados",prompt="Digite o nome: ")
    idade=simpledialog.askinteger(title="Cadastrar dados",prompt="Digite a idade ")
    if nome and idade!="":
        dados=(nome,idade,id)
        cursor.execute(sql,dados)
        conexion.commit()
        messagebox.showinfo('Situção do Cadastro','Cadastro atualizado')
        cursor.close()
        conexion.close
    else:
        messagebox.showerror('Situção do Cadastro','Cadastro não Atualizado')


def delete():
    conexion=psycopg2.connect(user='postgres', password='password', host='127.0.0.1', port='5432', database='db')

    cursor=conexion.cursor()
    id=simpledialog.askinteger(title="Cadastrar dados",prompt="Digite o id para apagar ")
    sql='DELETE FROM pessoas WHERE id=%s'
    cursor.execute(sql,(id,))
    conexion.commit()
    messagebox.showinfo('Progresso da execução','Dados excluídos')
    cursor.close()
    conexion.close()


def read():
    conexion=psycopg2.connect(user='postgres', password='password', host='127.0.0.1', port='5432', database='db')
    cursor = conexion.cursor()
    sql = 'SELECT * FROM pessoas WHERE id = %s'
    id = simpledialog.askinteger(title="Cadastrar dados", prompt="Digite o id para buscar ")
    cursor.execute(sql, (id,))
    registro = cursor.fetchone()
    conexion.close()

    # Verificar se o registro está vazio
    if registro is None:
        messagebox.showinfo("Dados não encontrados", "Nenhum dado foi encontrado.")
        return

    # Criar a janela
    root = tk.Tk()
    root.title("Dados Retornados")

    # Criar um rótulo para cada coluna do banco de dados
    labels = ['ID', 'Nome', 'Idade']
    for i, label in enumerate(labels):
        tk.Label(root, text=label).grid(row=6, column=i)

    # Preencher os dados na janela
    for i, value in enumerate(registro):
        tk.Label(root, text=value).grid(row=4, column=i)

    root.mainloop()

while True:
    print("\n-------CRUD-------")
    for i, main in enumerate(options_menu, start=1):
        print(f"{i}-{main}")
    opcao = input("Escolha uma opção (1-4): ")

    if opcao == "1":
        insert()
    elif opcao == "2":
        update()
    elif opcao == "3":
        delete()
    elif opcao == "4":
        read()
    elif opcao == "5":
        print("Encerrando o programa...")
        break
    else:
        print("Opção inválida. Digite novamente.")